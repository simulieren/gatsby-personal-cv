import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Flex } from 'rebass';
import { BodyText } from '../../typography/Typography';

export const SideNavContainer = styled(Flex)`
  display: none;

  @media screen and (min-width: 600px) {
    display: flex;
    position: fixed;
    top: 1rem;
  }
`;

export const SideNavItem = styled(BodyText)`
  position: relative;
  display: inline-block;
  cursor: pointer;
  transition: all 0.2s ease;
  width: auto;
  text-indent: 0 !important;
  font-weight: bold;

  &::before {
    content: '';
    position: absolute;
    z-index: -1;
    bottom: 0;
    left: 0;
    width: 100%;
    height: 50%;
    background: linear-gradient(
      to right,
      hsla(223, 100%, 50%, 0.15),
      hsla(223, 100%, 50%, 0.15)
    );
    transform: scale3d(1, 0, 1);
    transform-origin: 50% 100%;
    transition: transform 0.3s;
    transition-timing-function: cubic-bezier(0.7, 0, 0.3, 1);
  }

  &:hover::before {
    transform: scale3d(1, 1, 1);
    transform-origin: 50% 0%;
  }
`;

export const SideNav = props => {
  const { links } = props;

  const scroll = el => {
    el.current.scrollIntoView({ block: 'start', behavior: 'smooth' });
  };

  return (
    <SideNavContainer as="nav">
      <Flex p={4} as="nav" flexDirection={'column'}>
        {links.map((link, index) => (
          <SideNavItem
            onClick={() => scroll(link.ref)}
            py={1}
            my={1}
            key={index}
            fontSize={[2]}>
            {link.label}
          </SideNavItem>
        ))}
      </Flex>
    </SideNavContainer>
  );
};

SideNav.propTypes = {
  links: PropTypes.array.isRequired,
};
