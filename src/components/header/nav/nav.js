import React from 'react';
import styled from 'styled-components';
import { Flex } from 'rebass';
import {
  CodepenIcon,
  GitlabIcon,
  MailIcon,
  MessageIcon,
  LinkedInIcon,
  GitHubIcon,
  WebsiteIcon,
} from '../../icon';
import { BodyText } from '../../typography/Typography';

const NavBox = props => (
  <Flex
    as="a"
    mx={[3, 4]}
    alignItems="center"
    flexDirection="column"
    {...props}
  />
);
const NavLink = styled(NavBox)`
  position: relative;

  & svg {
    margin-bottom: 1rem;
  }

  & p {
    position: absolute;
    opacity: 0;
    transition: 0.5s ease;
    bottom: -2rem;
  }

  &:hover p {
    opacity: 1;
  }
`;

const Nav = () => (
  <Flex as="ul" flexWrap="wrap" justifyContent="center">
    <NavLink
      target="_blank"
      rel="noopener noreferrer"
      href="http://simonhalimonov.de/">
      <WebsiteIcon />
      <BodyText mb="0" as="p" fontSize={2}>
        Website
      </BodyText>
    </NavLink>
    <NavLink
      target="_blank"
      rel="noopener noreferrer"
      href="https://codepen.io/simulieren">
      <CodepenIcon />
      <BodyText mb="0" as="p" fontSize={2}>
        Codepen
      </BodyText>
    </NavLink>
    <NavLink
      target="_blank"
      rel="noopener noreferrer"
      href="https://github.com/simulieren">
      <GitHubIcon />
      <BodyText mb="0" as="p" fontSize={2}>
        GitHub
      </BodyText>
    </NavLink>
    <NavLink
      target="_blank"
      rel="noopener noreferrer"
      href="https://gitlab.com/simulieren">
      <GitlabIcon />
      <BodyText mb="0" as="p" fontSize={2}>
        GitLab
      </BodyText>
    </NavLink>
    <NavLink
      target="_blank"
      rel="noopener noreferrer"
      href="https://www.linkedin.com/in/simon-halimonov-745431181/">
      <LinkedInIcon />
      <BodyText mb="0" as="p" fontSize={2}>
        LinkedIn
      </BodyText>
    </NavLink>
    <NavLink href="mailto:hello@simonhalimonov.de">
      <MailIcon />
      <BodyText mb="0" as="p" fontSize={2}>
        Mail
      </BodyText>
    </NavLink>
    <NavLink
      target="_blank"
      rel="noopener noreferrer"
      href="https://wa.me/491738823363">
      <MessageIcon />
      <BodyText mb="0" as="p" fontSize={2}>
        WhatsApp
      </BodyText>
    </NavLink>
  </Flex>
);

export default Nav;
