import React from 'react';
import { Flex } from 'rebass';

export const Container = props => (
  <Flex
    p="4"
    flexDirection={['column', 'row']}
    alignItems="center"
    justifyContent="center"
    {...props}
  />
);
