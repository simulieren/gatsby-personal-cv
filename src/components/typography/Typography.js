import React from 'react';
import { Heading, Text } from 'rebass';

export const H2 = props => (
  <Heading
    as="h2"
    fontFamily="Tenor Sans"
    mb="4"
    fontSize="5"
    textAlign="center"
    {...props}>
    {props.children}
  </Heading>
);

export const H3 = props => (
  <Heading
    as="h3"
    fontFamily="Tenor Sans"
    my={[4, 5]}
    fontSize="3"
    style={{ textTransform: 'uppercase', letterSpacing: '.2em' }}
    {...props}>
    {props.children}
  </Heading>
);

export const BodyText = props => (
  <Text
    as="p"
    fontFamily="EB Garamond"
    lineHeight="1.7"
    fontSize={[3, 4]}
    style={{ wordSpacing: '.1em' }}
    mb="4"
    {...props}>
    {props.children}
  </Text>
);

export const Caption = props => (
  <Text
    as="span"
    fontFamily="Chivo"
    fontWeight="bold"
    style={{ textTransform: 'uppercase', fontStyle: 'normal' }}
    fontSize={[2]}
    letterSpacing="0.1em"
    {...props}>
    {props.children}
  </Text>
);
