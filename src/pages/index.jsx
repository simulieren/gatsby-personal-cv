import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import Carousel from 're-carousel';

import 'typeface-tenor-sans';
import 'typeface-chivo';
import 'typeface-eb-garamond';

import styled from 'styled-components';
import { Box, Flex, Text } from 'rebass';

import Layout from 'components/layout';
import { graphql } from 'gatsby';
import Nav from '../components/header/nav/nav';
import { SideNav } from '../components/header/nav/SideNav';
import { H2, H3, BodyText, Caption } from '../components/typography/Typography';

const ListItem = styled.li`
  position: relative;
  word-spacing: 0.1em;

  &::before {
    content: '';
    display: inline-block;
    position: absolute;
    height: 1px;
    width: 40px;
    background: black;
    top: 20px;
  }

  & p {
    margin-left: 60px;
  }
`;

const DetailItem = props => {
  return (
    <Flex alignItems="baseline">
      <Box width={[1 / 4]}>
        <Caption as="div">{props.label}</Caption>
      </Box>
      <Box width={[3 / 4]}>
        <BodyText>{props.children}</BodyText>
      </Box>
    </Flex>
  );
};

DetailItem.propTypes = {
  label: PropTypes.string,
  children: PropTypes.element,
};

const Divider = styled(Box)`
  width: 100%;
  height: 2px;
  background: #eee;
`;

const CarouselContainer = styled.div`
  position: relative;
  height: ${props => props.height || '80vh'};
`;

const Index = () => {
  const experience = useRef(null);
  const skills = useRef(null);
  const projects = useRef(null);
  const contact = useRef(null);

  return (
    <Layout>
      <SideNav
        links={[
          { ref: experience, label: 'Experience' },
          { ref: skills, label: 'Skills' },
          { ref: projects, label: 'Projects' },
          { ref: contact, label: 'Contact' },
        ]}
      />
      <Flex alignItems="center" justifyContent="center" flexDirection="column">
        <Box p={[4]}>
          <H3 textAlign="center">CV Profile</H3>
          <H2 textAlign="center">Simon Halimonov</H2>
          <Flex justifyContent="center" flexDirection="column">
            <Caption mb="2" textAlign="center">
              Digital Product Designer and Frontend Developer
            </Caption>
            <BodyText fontSize={[2]} as="p" textAlign="center">
              Available for design and development work.
            </BodyText>
          </Flex>
        </Box>
      </Flex>
      <Flex
        ref={experience}
        px="3"
        py={[3, 5]}
        justifyContent="center"
        flexDirection="column">
        <H2>Professional Experience</H2>
      </Flex>
      <Flex px="3" justifyContent="center">
        <Box as="ul" width={[1, 5 / 7, 5 / 7, 4 / 8]}>
          <ListItem>
            <BodyText>
              <em>Frontend developer</em> with focus on cutting edge
              <em> HTML, CSS, JavaScript, TypeScript </em>
              and associated frameworks such as{' '}
              <em>React.js, Next.js, Gatsby.js</em> and other{' '}
              <em>JavaScript</em>
              development tools.
            </BodyText>
          </ListItem>
          <ListItem>
            <BodyText>
              Experience as a <em>UI designer</em> for <em>small startups</em>{' '}
              and <em>international corporations</em> by following small and
              large scale <em>style guides</em> and creating{' '}
              <em>design systems</em> for{' '}
              <em>international multilingual websites</em> and{' '}
              <em>digital applications.</em>
            </BodyText>
          </ListItem>
          <ListItem>
            <BodyText>
              Professional knowledge in <em>conception, design, development</em>{' '}
              of
              <em>websites</em> and <em>digital applications</em>.
            </BodyText>
          </ListItem>
          <ListItem>
            <BodyText>
              Knowledge and experience of the React ecosystem and popular
              libraries like{' '}
              <em>Redux, styled-components, Next.js, Gatsby.js, Rebass</em> and
              more. Understanding of{' '}
              <em>different patterns, approaches and techniques</em> to provide
              the best solution for each use case.
            </BodyText>
          </ListItem>
          <ListItem>
            <BodyText>
              Knowledge and experience of the React ecosystem and popular
              libraries like{' '}
              <em>Redux, styled-components, Next.js, Gatsby.js, Rebass</em> and
              more. Understanding of{' '}
              <em>different patterns, approaches and techniques</em> to provide
              the best solution for each use case.
            </BodyText>
          </ListItem>
          <ListItem>
            <BodyText>
              Experience in <em>connecting frontend applications</em> to{' '}
              <em>APIs (REST, GraphQL, Firebase, Socket.io).</em>
            </BodyText>
          </ListItem>
          <ListItem>
            <BodyText>
              Development of applications and APIs with <em>Node.js</em> in the
              backend.
            </BodyText>
          </ListItem>
          <ListItem>
            <BodyText>
              Professional experience with design tools like{' '}
              <em>Figma, Sketch, Framer X and Adobe CC</em> to{' '}
              <em>
                create sketches, wireframes, wireflows and mockups and
                prototypes.
              </em>
            </BodyText>
          </ListItem>
        </Box>
      </Flex>
      <Flex
        ref={skills}
        px="3"
        py={[3, 5]}
        justifyContent="center"
        flexDirection="column">
        <H2>Technical Skills</H2>
      </Flex>
      <Flex px="3" justifyContent="center">
        <Box as="ul" width={[1, 5 / 7, 5 / 7, 4 / 8]}>
          <H3>Programming Skills</H3>
          <ListItem>
            <BodyText>
              <Caption as="div">JavaScript</Caption>
              ES2019, TypeScript, React / Redux, NodeJS, Express, Mongoose,
              Grunt, Gulp, Babel, jQuery, Webpack, Gatsby, Next, Material UI,
              styled-components, react-admin ...
            </BodyText>
          </ListItem>
          <ListItem>
            <BodyText>
              <Caption as="div">CSS</Caption>
              CSS, LESS, SCSS, PostCSS, Bootstrap 4, ZURB Foundation
            </BodyText>
          </ListItem>
          <ListItem>
            <BodyText>
              <Caption as="div">HTML</Caption>
              HTML4, HTML5
            </BodyText>
          </ListItem>
          <ListItem>
            <BodyText>
              <Caption as="div">Databases</Caption>
              MongoDB, Firebase
            </BodyText>
          </ListItem>
          <H3>Design Skills</H3>
          <ListItem>
            <BodyText>
              <Caption as="div">User Interface Design</Caption>
              Responsive Webdesign, Design System, Design Styleguides,
              Responsive Newsletter Design, Code Prototyping
            </BodyText>
          </ListItem>
          <ListItem>
            <BodyText>
              <Caption as="div">User Experience Design</Caption>
              Wireframing, Wireflows, Design Prototyping
            </BodyText>
          </ListItem>
          <H3>General Skills</H3>
          <ListItem>
            <BodyText>
              <Caption as="div">Workflow</Caption>
              Git flow, GitHub flow, GitLab flow, Agile, Scrum, Kanban, Design
              Sprints
            </BodyText>
          </ListItem>
          <ListItem>
            <BodyText>
              <Caption as="div">Tools</Caption>
              Visual Studio Code, Docker, Jira, Confluence, Trello, Adobe CC,
              Sketch, Figma, Framer X, Invision, Prettier, ESLint, GitKraken ...
            </BodyText>
          </ListItem>
        </Box>
      </Flex>

      <Flex
        ref={projects}
        px="3"
        py={[3, 5]}
        justifyContent="center"
        flexDirection="column">
        <H2>Project History</H2>
      </Flex>

      <Flex px="3" justifyContent="center" alignItems="flex-start">
        <Box as="ul" width={[1, 5 / 7, 5 / 7, 4 / 8]}>
          <video src="./MyStudiolo_Desktop_Preview.mp4" controls loop />
          <BodyText fontSize="2" textAlign="center" my="2">
            Desktop landingpage design created in Framer X with React code
            components <i>(Work in progress)</i>{' '}
          </BodyText>
          <Caption as="div" my="4">
            2019 — Now
          </Caption>
          <DetailItem label="Client">
            MyStudiolo <i>(WIP)</i>
          </DetailItem>
          <DetailItem label="Role">
            Freelance UI Designer <i>(Remote)</i>
          </DetailItem>
          <DetailItem label="Tech">
            Framer X, TypeScript, React, GatsbyJS
          </DetailItem>
          <DetailItem label="Task">
            Designing the whole experience of a new product startup for art
            collectors in Paris, France. Together with a group of freelancers
            from the{' '}
            <a
              target="_blank"
              rel="noopener noreferrer"
              href="https://www.jungleprogram.com/">
              JungleProgram
            </a>
            . I am creating the UI design and frontend components for the
            upcoming product experience.
          </DetailItem>

          <Divider mb={[4, 5]} />

          <video src="./howfm_CMS.mp4" controls loop />
          <BodyText fontSize="2" textAlign="center" my="2">
            Admin CMS created with React, Redux and written in TypeScript
          </BodyText>
          <Caption as="div" my="4">
            2019 — Now
          </Caption>
          <DetailItem label="Client">
            <a
              target="_blank"
              rel="noopener noreferrer"
              href="https://www.soundreply.com/">
              SoundReply GmbH
            </a>
          </DetailItem>
          <DetailItem label="Role">
            Freelance Frontend Developer <i>(Remote)</i>
          </DetailItem>
          <DetailItem label="Tech">
            Firebase, Typescript, ES2019, React, Redux, Gatsby.js, react-admin,
            GitLab
          </DetailItem>
          <DetailItem label="Task">
            Creating the frontend of the custom CMS experience. Supporting a
            start up run by many Ex-Trivago engineers and helping them create
            their vision to provide a perfect user experience including a
            interactive backend for a machine learning and natural language
            processing application. Mostly written in Typescript in the Google
            Cloud as part of a Flutter / Firebase application.
          </DetailItem>

          <Divider mb={[4, 5]} />

          <Caption as="div" my="4">
            Dezember 2017 — Dezember 2018
          </Caption>
          <DetailItem label="Client">
            <a
              target="_blank"
              rel="noopener noreferrer"
              href="https://digitalcareerinstitute.org/">
              Digital Career Institute gGmbH
            </a>{' '}
          </DetailItem>
          <DetailItem label="Role">
            Freelance Fullstack JavaScript Dozent
          </DetailItem>
          <DetailItem label="Tech">
            HTML, CSS, JavaScript, React, Redux, NodeJS, MongoDB, Git, GitHub
          </DetailItem>
          <DetailItem label="Task">
            Teaching a full year course on Fullstack JavaScript Development. I
            have thaught over a hundred students starting from the very basics
            of web development until they are ready to work with real world tech
            in companies in Germany. Doing workshops with the topic digital
            design in different cities across Germany.
          </DetailItem>

          <Divider mb={[4, 5]} />

          <CarouselContainer>
            <Carousel auto loop>
              <div>
                <BodyText fontSize="2" textAlign="center" mb="2">
                  UI / UX Design für beka GmbH
                </BodyText>
                <img src="./beka-1.png" alt="" />
              </div>
              <div>
                <BodyText fontSize="2" textAlign="center" mb="2">
                  UI / UX Design für beka GmbH
                </BodyText>
                <img src="./beka-2.png" alt="" />
              </div>
              <div>
                <BodyText fontSize="2" textAlign="center" mb="2">
                  UI / UX Design für beka GmbH
                </BodyText>
                <img src="./beka-3.png" alt="" />
              </div>
              <div>
                <BodyText fontSize="2" textAlign="center" mb="2">
                  UI / UX Design für TechnoCargo Logistik GmbH u. Co. KG
                </BodyText>
                <img src="./technocargo-1.png" alt="" />
              </div>
              <div>
                <BodyText fontSize="2" textAlign="center" mb="2">
                  UI / UX Design für TechnoCargo Logistik GmbH u. Co. KG
                </BodyText>
                <img src="./technocargo-2.png" alt="" />
              </div>
            </Carousel>
          </CarouselContainer>

          <Caption as="div" my="4">
            Juni 2017 — November 2017
          </Caption>
          <DetailItem label="Client">
            <a
              target="_blank"
              rel="noopener noreferrer"
              href="https://www.allefreiheit.de/">
              alle freiheit GmbH
            </a>
          </DetailItem>
          <DetailItem label="Role">
            Fulltime Junior Art Director und Frontend Developer
          </DetailItem>
          <DetailItem label="Tech">
            HTML, CSS, JavaScript, WordPress, Joomla, PHP, Git, GitHub, Adobe XD
          </DetailItem>
          <DetailItem label="Task">
            Working as a designer and developer on various projects for regional
            clients. My tasks started at the conception and ended at the
            technical realization of the project with ongoing maintenance.
          </DetailItem>
          <DetailItem label="Clients">
            <a
              target="_blank"
              rel="noopener noreferrer"
              href="http://www.beka.de/">
              beka GmbH
            </a>
            ,{' '}
            <a
              target="_blank"
              rel="noopener noreferrer"
              href="https://fuka.de/de/">
              FUKA GmbH
            </a>
            ,{' '}
            <a
              target="_blank"
              rel="noopener noreferrer"
              href="https://folienjargon.de/">
              Folienjargon
            </a>
            , MACTAC, ...
          </DetailItem>

          <Divider mb={[4, 5]} />

          <CarouselContainer height={'50vh'}>
            <Carousel auto loop>
              <div>
                <BodyText fontSize="2" textAlign="center" mb="2">
                  UI Design für EIZO Europe GmbH
                </BodyText>
                <img src="./eizo-1.png" alt="" />
              </div>
              <div>
                <BodyText fontSize="2" textAlign="center" mb="2">
                  UI Design für EIZO Europe GmbH
                </BodyText>
                <img src="./eizo-2.png" alt="" />
              </div>
              <div>
                <BodyText fontSize="2" textAlign="center" mb="2">
                  UI Design für Covestro AG
                </BodyText>
                <img src="./covestro-1.png" alt="" />
              </div>
              <div>
                <BodyText fontSize="2" textAlign="center" mb="2">
                  UI Design für Covestro AG
                </BodyText>
                <img src="./covestro-2.png" alt="" />
              </div>
              <div>
                <BodyText fontSize="2" textAlign="center" mb="2">
                  UI Design für FUJIFILM Europe GmbH
                </BodyText>
                <img src="./myfujifilm-1.png" alt="" />
              </div>
            </Carousel>
          </CarouselContainer>

          <Caption as="div" my="4">
            Mai 2015 — Juni 2017
          </Caption>
          <DetailItem label="Client">
            <a
              target="_blank"
              rel="noopener noreferrer"
              href="https://www.twt-rb.de/">
              TWT reality bytes GmbH
            </a>
          </DetailItem>
          <DetailItem label="Role">Fulltime Junior Art Director</DetailItem>
          <DetailItem label="Tech">
            HTML, CSS, JavaScript, WordPress, Typo3, PHP, Gulp, Git, GitHub,
            Adobe CC
          </DetailItem>
          <DetailItem label="Task">
            Working as a designer and developer on various projects for small
            startups and big international coprorations. My tasks started at the
            design stage and ended at the technical prototype of the project
            with ongoing maintenance.
          </DetailItem>
          <DetailItem label="Clients">
            <a
              target="_blank"
              rel="noopener noreferrer"
              href="http://www.eizo.de/">
              EIZO
            </a>
            ,{' '}
            <a
              target="_blank"
              rel="noopener noreferrer"
              href="https://www.lekkerland.de/">
              Lekkerland GmbH
            </a>
            ,{' '}
            <a
              target="_blank"
              rel="noopener noreferrer"
              href="https://www.myfujifilm.de/start">
              FUJIFILM
            </a>
            ,{' '}
            <a
              target="_blank"
              rel="noopener noreferrer"
              href="https://www.covestro.com/de">
              Covestro
            </a>
            , ...
          </DetailItem>
        </Box>
      </Flex>

      <Flex
        ref={contact}
        mt={[5, 6]}
        p={3}
        as="section"
        justifyContent="center"
        alignItems="center"
        flexDirection="column">
        <H2>Contact</H2>
        <Text
          fontFamily="EB Garamond"
          lineHeight="1.7"
          fontSize={[3, 5]}
          mb="3"
          style={{ fontStyle: 'italic' }}
          as="a"
          href="https://wa.me/491738823363">
          <Caption mr={[3, 4]}>WhatsApp:</Caption>
          +49 173 882 336 3
        </Text>
        <Text
          fontFamily="EB Garamond"
          lineHeight="1.7"
          fontSize={[3, 5]}
          mb="5"
          style={{ fontStyle: 'italic' }}
          as="a"
          href="mailto:hello@simonhalimonov.de">
          <Caption mr={[3, 4]}>Mail:</Caption>
          hello@simonhalimonov.de
        </Text>
      </Flex>
      <Flex
        p={3}
        as="footer"
        justifyContent="center"
        alignItems="center"
        flexDirection="column">
        <Nav />
      </Flex>
      <Flex
        p={3}
        as="section"
        justifyContent="center"
        alignItems="center"
        flexDirection="column">
        <Text
          mt="5"
          as="p"
          fontSize="2"
          lineHeight="1.7"
          fontFamily="EB Garamond"
          width={[1, 1 / 2]}
          textAlign="center">
          The code used for this project can be found here:{' '}
          <a
            target="_blank"
            rel="noopener noreferrer"
            href="https://gitlab.com/simulieren/gatsby-personal-cv">
            GitLab Repository.
          </a>{' '}
          This project uses the following open-source font families:{' '}
          <a href="https://www.npmjs.com/package/typeface-tenor-sans">
            Tenor Sans
          </a>
          , <a href="https://www.npmjs.com/package/typeface-archivo">Archivo</a>{' '}
          and{' '}
          <a href="https://www.npmjs.com/package/typeface-eb-garamond">
            EB Garamond
          </a>
          . A list of all open-source packages can be found{' '}
          <a href="https://gitlab.com/simulieren/gatsby-personal-cv/blob/master/package.json">
            here
          </a>
          .
        </Text>
      </Flex>
    </Layout>
  );
};

Index.propTypes = {
  data: PropTypes.object.isRequired,
};

export default Index;

export const query = graphql`
  query HomepageQuery {
    homeJson {
      title
      content {
        childMarkdownRemark {
          html
          rawMarkdownBody
        }
      }
      gallery {
        title
        copy
        image {
          childImageSharp {
            fluid(maxHeight: 500, quality: 90) {
              ...GatsbyImageSharpFluid_withWebp
            }
          }
        }
      }
    }
  }
`;
