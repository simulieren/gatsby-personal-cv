const path = require('path');

module.exports = {
  siteTitle: `CV — Simon Halimonov`,
  siteTitleShort: `CV — Simon H.`,
  siteDescription: `Simon Halimonov — CV Profile`,
  siteUrl: `http://www.simonhalimonov.com`,
  themeColor: `#000`,
  backgroundColor: `#00f`,
  pathPrefix: null,
  logo: path.resolve(__dirname, 'src/images/icon.png'),
  social: {
    twitter: `gatsbyjs`,
    fbAppId: `966242223397117`,
  },
};
